package org.walli.service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.persist.PersistService;
import org.junit.Before;
import org.junit.Test;
import org.walli.di.PersistenceModule;
import org.walli.di.ServiceModule;
import org.walli.domain.Authentication;
import org.walli.domain.Source;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

import static org.junit.Assert.assertEquals;

/** Test class for {@link SourceService} */
public class SourceServiceTest {
    @Inject
    private SourceService service; // class under test
    @Inject
    private Provider<EntityManager> provider;
    private EntityTransaction transaction;

    @Before
    public void setUp() throws Exception {
        final Injector injector = Guice.createInjector(new PersistenceModule(), new ServiceModule());
        injector.getInstance(PersistService.class).start();
        injector.injectMembers(this);
        final EntityManager em = provider.get();
        transaction = em.getTransaction();
        final Source source = new Source();
        source.setName("name");
        source.setConsumerKey("consumerKey");
        source.setConsumerSecret("consumerSecret");
        source.setPrivateKey("privateKey");
        source.setOauthToken("oauthToken");
        source.setOauthSecret("oauthSecret");
        source.setUrl("http://localhost");
        source.setUsername("username");
        source.setPassword("password");
        source.setAuthentication(Authentication.none);

        transaction.begin();
        service.add(source);
        transaction.commit();
    }

    @Test
    public void shouldGetSources() throws Exception {
        assertEquals(1, service.sources().size());
    }

    @Test(expected = NoResultException.class)
    public void shouldThrowExceptionWhenGettingSourceByIdNotFound() throws Exception {
        service.sourceById(0L);
    }

    @Test
    public void shouldGetSourceById() throws Exception {
        final Source source = service.sourceById(1L);
        assertEquals(1L, source.getId(), 0);
        assertEquals("name", source.getName());
        assertEquals("consumerKey", source.getConsumerKey());
        assertEquals("consumerSecret", source.getConsumerSecret());
        assertEquals("privateKey", source.getPrivateKey());
        assertEquals("oauthToken", source.getOauthToken());
        assertEquals("oauthSecret", source.getOauthSecret());
        assertEquals("http://localhost", source.getUrl());
        assertEquals("username", source.getUsername());
        assertEquals("password", source.getPassword());
        assertEquals(300000, source.getCacheTime());
        assertEquals(Authentication.none, source.getAuthentication());
    }

    @Test
    public void shouldGetSourceByName() throws Exception {
        final Source source = service.sourceByName("name");
        assertEquals(1L, source.getId(), 0);
        assertEquals("name", source.getName());
        assertEquals("consumerKey", source.getConsumerKey());
        assertEquals("consumerSecret", source.getConsumerSecret());
        assertEquals("privateKey", source.getPrivateKey());
        assertEquals("oauthToken", source.getOauthToken());
        assertEquals("oauthSecret", source.getOauthSecret());
        assertEquals("http://localhost", source.getUrl());
        assertEquals("username", source.getUsername());
        assertEquals("password", source.getPassword());
        assertEquals(300000, source.getCacheTime());
        assertEquals(Authentication.none, source.getAuthentication());
    }

    @Test
    public void shouldUpdateSource() throws Exception {
        Source source = service.sourceById(1L);
        source.setName("updatedName");
        source.setConsumerKey("updatedConsumerKey");
        source.setConsumerSecret("updatedConsumerSecret");
        source.setPrivateKey("updatedPrivateKey");
        source.setOauthToken("updatedOauthToken");
        source.setOauthSecret("updatedOauthSecret");
        source.setUrl("http://updatedLocalhost");
        source.setUsername("updatedUsername");
        source.setPassword("updatedPassword");
        source.setCacheTime(100000);
        source.setAuthentication(Authentication.basic);
        service.update(source);

        source = service.sourceById(1L);
        assertEquals("updatedName", source.getName());
        assertEquals("updatedConsumerKey", source.getConsumerKey());
        assertEquals("updatedConsumerSecret", source.getConsumerSecret());
        assertEquals("updatedPrivateKey", source.getPrivateKey());
        assertEquals("updatedOauthToken", source.getOauthToken());
        assertEquals("updatedOauthSecret", source.getOauthSecret());
        assertEquals("http://updatedLocalhost", source.getUrl());
        assertEquals("updatedUsername", source.getUsername());
        assertEquals("updatedPassword", source.getPassword());
        assertEquals(100000, source.getCacheTime());
        assertEquals(Authentication.basic, source.getAuthentication());
    }

    @Test
    public void shouldDeleteSource() throws Exception {
        assertEquals(1, service.sources().size());

        transaction.begin();
        service.delete(1L);
        transaction.commit();

        assertEquals(0, service.sources().size());
    }
}
