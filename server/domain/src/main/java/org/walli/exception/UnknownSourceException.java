package org.walli.exception;

/** UnknownSourceException is thrown when a Source is not found. */
public class UnknownSourceException extends RuntimeException {

    /**
     * Constructor.
     * @param message The message.
     */
    public UnknownSourceException(final String message) {
        super(message);
    }
}
