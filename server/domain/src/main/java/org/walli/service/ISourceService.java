package org.walli.service;

import org.walli.domain.Source;

import java.util.List;

/** Service interface for sources. */
public interface ISourceService {
    /**
     * Gets all sources.
     * @return sources The sources.
     */
    List<Source> sources();

    /**
     * Gets the source.
     * @param id The id.
     * @return source The source
     */
    Source sourceById(final Long id);

    /**
     * Gets the source.
     * @param name The name.
     * @return source The source
     */
    Source sourceByName(final String name);

    /**
     * Add a source.
     * @param source The source.
     */
    void add(final Source source);

    /**
     * Update a source.
     * @param source The source.
     */
    void update(final Source source);

    /**
     * Delete the source matching the given id.
     * @param id The id.
     */
    void delete(final Long id);
}
