package org.walli.service;

import org.walli.domain.Source;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;

/** Source service implementation. */
@Singleton
public class SourceService implements ISourceService {
    @Inject
    private Provider<EntityManager> provider;

    /** {@inheritDoc}. */
    public List<Source> sources() {
        return (List<Source>) provider.get().createNamedQuery("Source.findAll").getResultList();
    }

    /** {@inheritDoc}. */
    public Source sourceById(final Long id) {
        return (Source) provider.get().createNamedQuery("Source.byId").setParameter("id", id).getSingleResult();
    }

    /** {@inheritDoc}. */
    public Source sourceByName(final String name) {
        return (Source) provider.get().createNamedQuery("Source.byName").setParameter("name", name).getSingleResult();
    }

    /** {@inheritDoc}. */
    public void add(final Source source) {
        provider.get().persist(source);
    }

    /**
     * {@inheritDoc}.
     * @TODO wipe other authentication values
     */
    public void update(final Source source) {
        final EntityManager em = provider.get();
        final Source found = sourceById(source.getId());
        found.setName(source.getName());
        found.setConsumerKey(source.getConsumerKey());
        if (!source.getConsumerSecret().equals("unchanged")) {
            found.setConsumerSecret(source.getConsumerSecret());
        }
        found.setPrivateKey(source.getPrivateKey());
        if (!source.getOauthToken().equals("unchanged")) {
            found.setOauthToken(source.getOauthToken());
        }
        if (!source.getOauthSecret().equals("unchanged")) {
            found.setOauthSecret(source.getOauthSecret());
        }
        found.setUsername(source.getUsername());
        if (!source.getPassword().equals("unchanged")) {
            found.setPassword(source.getPassword());
        }
        found.setCacheTime(source.getCacheTime());
        found.setUrl(source.getUrl());
        found.setAuthentication(source.getAuthentication());
        em.persist(found); // updated source.
    }

    /** {@inheritDoc}. */
    public void delete(final Long id) {
        final EntityManager em = provider.get();
        em.remove(sourceById(id));
    }

}
