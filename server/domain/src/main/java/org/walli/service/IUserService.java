package org.walli.service;

import org.walli.domain.User;

import java.util.List;

/** Service interface for Users. */
public interface IUserService {
    /**
     * Gets all Users.
     * @return Users The Users.
     */
    List<User> users();

    /**
     * Gets the User.
     * @param id The id.
     * @return User The User
     */
    User userById(final Long id);

    /**
     * Gets the User.
     * @param username The username.
     * @return User The User
     */
    User userByUsername(final String username);

    /**
     * Gets all admins.
     * @return Users The admins.
     */
    List<User> admins();

    /**
     * Add a User.
     * @param User The User.
     */
    void add(final User User);

    /**
     * Update a User.
     * @param User The User.
     */
    void update(final User User);


    /**
     * Delete the User matching the given id.
     * @param id The id.
     */
    void delete(final Long id);

}
