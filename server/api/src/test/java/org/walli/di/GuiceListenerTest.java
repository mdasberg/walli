package org.walli.di;

import com.google.inject.Injector;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.walli.cache.SourceCache;
import org.walli.service.IProjectService;
import org.walli.service.ISourceService;
import org.walli.service.ProjectService;
import org.walli.service.SourceService;

import javax.inject.Inject;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.isA;
import static org.powermock.api.mockito.PowerMockito.whenNew;

/** Test class for {@link GuiceListener}. */
@RunWith(PowerMockRunner.class)
@PrepareForTest({GuiceListener.class, WebModule.class, Properties.class})
public class GuiceListenerTest {
    private GuiceListener listener; // class under test

    @Inject
    private ISourceService sourceService;
    @Inject
    private IProjectService projectService;
    @Inject
    private SourceCache cache;
    @Inject
    private WebSecurityManager manager;
    @Mock
    private Properties properties;

    @Test
    public void shouldInject() throws Exception {
        listener = new GuiceListener();
        final Injector injector = listener.getInjector();
        injector.injectMembers(this);

        assertNotNull(projectService);
        assertTrue(projectService instanceof ProjectService);
        assertNotNull(sourceService);
        assertTrue(sourceService instanceof SourceService);
        assertNotNull(cache);
        assertTrue(cache instanceof SourceCache);
        assertNotNull(manager);
        assertTrue(manager instanceof DefaultWebSecurityManager);
    }

    @Test
    public void shouldThrowExceptionWhenPropertiesCannotBeFound() throws Exception {
        whenNew(Properties.class).withNoArguments().thenReturn(properties);
        doThrow(IOException.class).when(properties).load(isA(InputStream.class));

        try {
            listener = new GuiceListener();
            final Injector injector = listener.getInjector();
            injector.injectMembers(this);
            fail();
        } catch (Exception e) {

        }
    }
}
