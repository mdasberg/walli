package org.walli.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.walli.domain.User;
import org.walli.service.IUserService;
import org.walli.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import java.util.Arrays;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

/** Test class for {@link UserResource}. */
@RunWith(PowerMockRunner.class)
@PrepareForTest({AuthenticationResource.class, UserService.class})
public class UserResourceTest {
    @Inject
    private UserResource resource;
    @Mock
    private IUserService service;
    @Mock
    private User user;

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(AuthenticationResource.class); // As it is a Jax-rs resource it will be picked up.
                bind(IUserService.class).toInstance(service);
            }
        });
        injector.injectMembers(this);

    }

    @Test
    public void shouldGetUsers() throws Exception {
        resource.users();

        verify(service, times(1)).users();
    }

    @Test
    public void shouldGetUser() throws Exception {
        when(service.userById(isA(Long.class))).thenReturn(user);
        final Response response = resource.user(1L);
        assertEquals(user, ((GenericEntity) response.getEntity()).getEntity());
    }

    @Test
    public void shouldAddUser() throws Exception {
        resource.create(user);

        verify(service, times(1)).add(eq(user));
    }

    @Test
    public void shouldUpdateUser() throws Exception {
        resource.update(1L, user);

        verify(service, times(1)).update(eq(user));
    }

    @Test
    public void shouldNotDeleteUser() throws Exception {
        when(service.userById(isA(Long.class))).thenReturn(user);
        resource.delete(1L);

        verify(service, never()).delete(eq(1L));
    }

    @Test
    public void shouldDeleteUser() throws Exception {
        when(service.userById(isA(Long.class))).thenReturn(user);
        when(service.admins()).thenReturn(Arrays.asList(new User[]{user, user}));
        when(user.isAdmin()).thenReturn(false).thenReturn(true);
        resource.delete(1L);

        verify(service, times(1)).delete(eq(1L));
    }
}
