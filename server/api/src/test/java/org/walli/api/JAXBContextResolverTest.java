package org.walli.api;

import com.sun.jersey.api.json.JSONJAXBContext;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBContext;

import static org.junit.Assert.assertTrue;

/** Test class for{@link JAXBContextResolver}. */
public class JAXBContextResolverTest {
    private JAXBContextResolver resolver; // class under test

    @Before
    public void setUp() throws Exception {
        resolver = new JAXBContextResolver();
    }

    @Test
    public void shouldContainProjectAsMappedEntity() throws Exception {
        final JAXBContext context = resolver.getContext(null);
        assertTrue(context instanceof JSONJAXBContext);
        assertTrue(((JSONJAXBContext) context).getJSONConfiguration().getArrays().contains("project"));
        assertTrue(((JSONJAXBContext) context).getJSONConfiguration().getArrays().contains("violation"));
    }
}
