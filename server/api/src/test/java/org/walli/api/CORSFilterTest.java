package org.walli.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.util.Properties;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

/** Test class for {@link CORSFilter}. */
@RunWith(MockitoJUnitRunner.class)
public class CORSFilterTest {
    @Inject
    private CORSFilter filter; // class under test.

    @Mock
    private ServletRequest request;

    @Mock
    private ServletResponse response;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private Properties properties;

    @Mock
    private FilterChain chain;

    @Before
    public void setUp() throws Exception {
        final Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(Properties.class).annotatedWith(Names.named("walli")).toInstance(properties);
            }
        });
        injector.injectMembers(this);
        filter.init(null);
    }

    @Test
    public void shouldNotAddHeaders() throws Exception {
        filter.doFilter(request, response, chain);
        verify(properties, never()).get("access-control-allow-origin");
    }

    @Test
    public void shouldAddHeaders() throws Exception {
        filter.doFilter(request, httpServletResponse, chain);
        verify(properties, times(1)).get("access-control-allow-origin");
        verify(httpServletResponse, times(1)).addHeader(eq("Access-Control-Allow-Origin"), anyString());
        verify(httpServletResponse, times(1)).addHeader(eq("Access-Control-Allow-Methods"), anyString());
        verify(httpServletResponse, times(1)).addHeader(eq("Access-Control-Allow-Headers"), anyString());
        verify(httpServletResponse, times(1)).addHeader(eq("Access-Control-Max-Age"), anyString());
    }

    @After
    public void tearDown() throws Exception {
        filter.destroy();

    }
}
