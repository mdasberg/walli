package org.walli.api;

import com.google.inject.Singleton;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.api.json.JSONJAXBContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.walli.domain.Configuration;
import org.walli.domain.Project;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;

/**
 * JAXBContextResolver fixes the single element array result. Since Jersey doesn't return a list
 * but a single object when the list contains only one element, this class fixes the return type.
 */
@Provider
@Singleton
public class JAXBContextResolver implements ContextResolver<JAXBContext> {
    private static final Logger LOG = LoggerFactory.getLogger(JAXBContextResolver.class);
    private JAXBContext context;
    private Class[] types = {Project.class};

    /**
     * Default constructor.
     * @throws Exception The exception.
     */
    public JAXBContextResolver() throws Exception {
        LOG.debug("Initializing JAXBContextResolver.");
        this.context = new JSONJAXBContext(
                JSONConfiguration
                        .mapped()
                        .rootUnwrapping(true)
                        .arrays("project", "violation")
                        .build(), types);
    }

    /**
     * Gets the context.
     * @param objectType The object Type.
     * @return context The context.
     */
    public JAXBContext getContext(Class<?> objectType) {
        return context;
    }
}