package org.walli.api;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import static org.walli.api.WalliMediaTypes.CONFIGURATION_VND;

/** Configuration Vnd provider. */
@Provider
@Consumes({MediaType.APPLICATION_JSON, "text/json"})
@Produces({MediaType.APPLICATION_JSON, "text/json", CONFIGURATION_VND})
public class ConfigurationVndProvider extends JacksonJsonProvider {
}
