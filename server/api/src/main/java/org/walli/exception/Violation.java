package org.walli.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/** The Violation Object is used for returning a field violation to the user. */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Violation {
    private String field;
    private String violation;

    /**
     * Constructor.
     * @param field     The field.
     * @param violation The violation.
     */
    public Violation(final String field, final String violation) {
        this.field = field;
        this.violation = violation;
    }

    /**
     * Gets the field.
     * @return field The field.
     */
    public String getField() {
        return field;
    }

    /**
     * Gets the violation.
     * @return violation The violation.
     */
    public String getViolation() {
        return violation;
    }
}