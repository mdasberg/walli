package org.walli.exception;

import com.google.inject.Singleton;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.ArrayList;
import java.util.List;

/** ConstraintViolation exceptionMapper takes care of returning the constrained validations. */
@Singleton
@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(final ConstraintViolationException e) {
        final Response.Status status;
        List<Violation> violations = new ArrayList<Violation>();
        for (ConstraintViolation cv : e.getConstraintViolations()) {
            violations.add(new Violation(cv.getPropertyPath().toString(), cv.getMessage()));
        }
        final GenericEntity entity = new GenericEntity<List<Violation>>(violations) {
        };
        status = Response.Status.BAD_REQUEST;
        return Response.status(status).entity(entity).build();
    }
}