package org.walli.cache;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.walli.connector.AuthenticationInformation;

/**
 * Proxy request.
 * @param <T> The authentication information
 */
public class ProxyRequest<T extends AuthenticationInformation> {
    private String url;
    private T information;

    /**
     * Constructor.
     * @param url         The url.
     * @param information The authentication information.
     */
    public ProxyRequest(final String url, final T information) {
        this.url = url;
        this.information = information;
    }

    /**
     * Gets the url.
     * @return url The url.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets the authentication information.
     * @return information The authentication information.
     */
    public T getInformation() {
        return information;
    }

    @Override
    public boolean equals(final Object o) {
        return EqualsBuilder.reflectionEquals(this, o, new String[]{"information"});
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(url);
    }
}
