package org.walli.cache;

import javax.ws.rs.core.Response;

/** ProxyResponse is used for caching a response object for a specific period. */
public class ProxyResponse {
    private long validUntil;
    private Response response;

    /**
     * Constructor.
     * @param validUntil Timestamp that indicate till when the response is valid.
     * @param response   The response.
     */
    public ProxyResponse(long validUntil, Response response) {
        this.validUntil = validUntil;
        this.response = response;
    }

    /**
     * Gets the response.
     * @return response The response.
     */
    public Response getResponse() {
        return response;
    }

    /**
     * Indicates if teh given response is still valid.
     * @return <code>true</code> if valid, else <code>false</code>.
     */
    public boolean isValid() {
        return System.currentTimeMillis() < validUntil;
    }
}
