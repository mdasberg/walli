package org.walli.cache;

import javax.inject.Singleton;
import java.util.Hashtable;

/**
 * ProxyCache is a caching provider that returns non-expired proxy responses that match given the proxy requests
 * If a proxy response has expired, it will be removed from the cache.
 */
@Singleton
public final class ProxyCache {

    /** Holds all sources. */
    private Hashtable<ProxyRequest, ProxyResponse> cache = new Hashtable<ProxyRequest, ProxyResponse>();

    /** Clear the cache. */
    public void clear() {
        cache.clear();
    }

    /**
     * Get the cached response for the given request.
     * Only responses that are still valid are returned.
     * @param request The request.
     * @return response The response.
     */
    public ProxyResponse getCachedResponse(final ProxyRequest request) {
        final ProxyResponse response = cache.get(request);
        if (response != null && !response.isValid()) {
            cache.remove(request);
            return null;
        }
        return response;
    }

    /**
     * Adds the given request/response to the cache.
     * @param request  The request.
     * @param response The response.
     */
    public void add(final ProxyRequest request, final ProxyResponse response) {
        cache.put(request, response);
    }
}
