package org.walli.transformer;

import java.util.Date;

/**
 *
 */
public class RssEntry {
    private final Date date;
    private final String title;

    RssEntry(Date date, String title) {
        this.date = date;
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RssEntry rssEntry = (RssEntry) o;

        if (date != null ? !date.equals(rssEntry.date) : rssEntry.date != null) return false;
        if (title != null ? !title.equals(rssEntry.title) : rssEntry.title != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        return result;
    }
}
