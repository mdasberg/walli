package org.walli.cache;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.walli.di.ConnectModule;
import org.walli.domain.Source;
import org.walli.exception.UnknownSourceException;
import org.walli.service.ISourceService;

import javax.inject.Inject;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.*;

/** Test class for {@link SourceCache}. */
@RunWith(MockitoJUnitRunner.class)
public class SourceCacheTest {
    private static final String SOURCE = "source";
    @Inject
    private SourceCache cache;
    @Mock
    private ISourceService service;
    @Mock
    private Source source;
    @Mock
    private Source otherSource;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        final Injector injector = Guice.createInjector(new ConnectModule(), new AbstractModule() {
            @Override
            protected void configure() {
                bind(ISourceService.class).toInstance(service);
            }
        });
        injector.injectMembers(this);
    }

    @Test
    public void shouldThrowExceptionWhenSourceCannotBeLoadedFromService() throws Exception {
        thrown.expect(UnknownSourceException.class);
        thrown.expectMessage("No Source with name [" + SOURCE + "].");
        cache.getSource(SOURCE);
    }


    @Test
    public void shouldGetSource() throws Exception {
        when(service.sourceByName(SOURCE)).thenReturn(source);
        cache.getSource(SOURCE);
        verify(service, times(1)).sourceByName(SOURCE);
    }

    @Test
    public void shouldUpdateSource() throws Exception {
        when(service.sourceByName(SOURCE)).thenReturn(source).thenReturn(otherSource);
        final Source found = cache.getSource(SOURCE);
        cache.updateSource(SOURCE);
        assertFalse(found.equals(cache.getSource(SOURCE)));
        verify(service, times(2)).sourceByName(SOURCE);
    }

    @Test
    public void shouldDeleteSource() throws Exception {
        when(service.sourceByName(SOURCE)).thenReturn(source).thenReturn(otherSource);
        final Source found = cache.getSource(SOURCE);
        cache.deleteSource(SOURCE);
        assertFalse(found.equals(cache.getSource(SOURCE)));
        verify(service, times(2)).sourceByName(SOURCE);
    }

    @Test
    public void shouldGetCachedSource() throws Exception {
        when(service.sourceByName(SOURCE)).thenReturn(source).thenReturn(otherSource);
        final Source found = cache.getSource(SOURCE);
        assertTrue(found.equals(cache.getSource(SOURCE)));
        verify(service, times(1)).sourceByName(SOURCE);
    }
}
