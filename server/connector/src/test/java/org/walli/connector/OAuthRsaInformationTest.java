package org.walli.connector;

import junit.framework.Assert;
import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.security.GeneralSecurityException;

/** Test class for {@link OAuthRsaInformation} */
public class OAuthRsaInformationTest {
    private static final String BEGIN = "-----BEGIN PRIVATE KEY-----";
    private static final String END = "-----END PRIVATE KEY-----";
    private static final String BEGIN_OR_END_TAG_MISSING = "Private key is missing required BEGIN PRIVATE KEY or END PRIVATE KEY tags.";
    private static final String CONSUMER_KEY = "consumerKey";
    private static final String OAUTH_TOKEN = "oauthToken";
    private static final String PRIVATE_KEY = "privateKey";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void shouldThrowExceptionWhenBeginTagIsNotFound() throws Exception {
        thrown.expect(GeneralSecurityException.class);

        thrown.expectMessage(BEGIN_OR_END_TAG_MISSING);
        new OAuthRsaInformation(CONSUMER_KEY, PRIVATE_KEY + END, OAUTH_TOKEN);
    }

    @Test
    public void shouldThrowExceptionWhenEndTagIsNotFound() throws Exception {
        thrown.expect(GeneralSecurityException.class);
        thrown.expectMessage(BEGIN_OR_END_TAG_MISSING);
        new OAuthRsaInformation(CONSUMER_KEY, BEGIN + PRIVATE_KEY, OAUTH_TOKEN);
    }

    @Test
    public void shouldThrowExceptionWhenBeginAndEndTagAreNotFound() throws Exception {
        thrown.expect(GeneralSecurityException.class);
        thrown.expectMessage(BEGIN_OR_END_TAG_MISSING);
        new OAuthRsaInformation(CONSUMER_KEY, PRIVATE_KEY, OAUTH_TOKEN);
    }

    @Test
    public void shouldThrowExceptionWhenEndTagIsBeforeBeginTag() throws Exception {
        thrown.expect(GeneralSecurityException.class);
        thrown.expectMessage("END PRIVATE KEY tag found before BEGIN PRIVATE KEY tag.");
        new OAuthRsaInformation(CONSUMER_KEY, END + PRIVATE_KEY + BEGIN, OAUTH_TOKEN);
    }

    @Test
    public void shouldInitialize() throws Exception {
        final String privateKey = IOUtils.toString(OAuthRsaInformationTest.class.getResourceAsStream("/myrsakey.pk8"));
        final OAuthRsaInformation information = new OAuthRsaInformation(CONSUMER_KEY, privateKey, OAUTH_TOKEN);
        Assert.assertEquals(CONSUMER_KEY, information.getConsumerKey());
        Assert.assertNotNull(information.getPrivateKey());
        Assert.assertEquals(OAUTH_TOKEN, information.getOauthToken());
    }

}
