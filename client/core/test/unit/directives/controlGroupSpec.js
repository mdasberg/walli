"use strict";

/* jasmine specs for the control-group directive, */
describe('The controlGroup directive', function () {
    var element, form;

    beforeEach(function() {
        module('core');

        inject(function($rootScope, $compile) {
            var $scope = $rootScope;
            element = angular.element(
                '<form name="form">' +
                '   <control-group label="the_label" id="the_id">' +
                '       <input type="text" id="the_id" name="the_id" ng-model="model.value" required=""/> ' +
                '   </control-group>' +
                '</form>'
            );
            $scope.model = {value: 'some_value'};
            $compile(element)($scope);
            $scope.$digest();
            form = $scope.form;
        })
    });

    it("should replace the control-group element", function() {
        var divs = element.find('div');
        expect(divs.length).toBe(2);
        expect(divs.eq(0).attr('class')).toContain('form-group');
    });

    it("should have the correct label", function() {
        var labels = element.find('label');
        expect(labels.length).toBe(1);
        expect(labels.eq(0).text()).toEqual('the_label')
    });

    it("should have the correct controls", function() {
        var divs = element.find('div');
        expect(divs.length).toBe(2);
        expect(divs.eq(1).attr('class')).toEqual('controls col-lg-10');
        expect(divs.eq(1).find('input').attr('id')).toEqual('the_id');
    });

    it('should invalidate form', function () {
        form.the_id.$setViewValue();
        expect(form.$valid).toBe(false);
    });

    it('should validate form', function () {
        form.the_id.$setViewValue('some_value');
        expect(form.$valid).toBe(true);
    });
});
