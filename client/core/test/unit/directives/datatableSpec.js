"use strict";

/* jasmine specs for the datatable directive, */
describe('The datatable directive', function () {
    var scope, compile, factory;

    var data = [
        {
            value: '100%',
            title: 'example 1',
            timestamp: 288334800000
        },
        {
            value: '85%',
            title: 'example 2',
            timestamp: 1167714000000
        },
        {
            value: '70%',
            title: 'example 3',
            timestamp: 1225342800000
        }
    ];

    beforeEach(function () {
        module('core');

        inject(function ($rootScope, $compile, modelFactory) {
            scope = $rootScope;
            compile = $compile;
            factory = modelFactory;
        })
    });

    describe('that is provided with valid data', function () {
        var element;

        beforeEach(function () {
            scope.config = factory.createDatatableConfig()
                .addColumn('value', 'Value', 'icon', '{{row.value}}', true, 3)
                .addColumn('', 'Build', '', '#{{row.title}}', true)
                .addColumn('timestamp', 'Build date', '', '{{row.timestamp | date:\'dd MMM `yy - HH:mm:ss\'}}', true)
                .addRows(data);

            element = angular.element(
                '<div>' +
                '   <datatable config="config" class="table table-normal"/>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();
        });

        it("should set the correct style class", function () {
            var table = element.find('table');
            expect(table.length).toBe(1);
            expect(table.eq(0).attr('class')).toContain('table table-normal');
        });

        it("should set the correct number of columns", function () {
            var table = element.find('thead');
            var trs = table.find('tr');
            expect(trs.length).toBe(1);
            var tds = trs.find('td');
            expect(tds.length).toBe(3);
        });

        it("should set the correct colspan in the header", function () {
            var table = element.find('thead');
            var trs = table.find('tr');
            expect(trs.length).toBe(1);
            var tds = trs.find('td');
            expect(tds.eq(0).attr('colspan')).toBe('3');
            expect(tds.eq(1).attr('colspan')).toBe('1');
            expect(tds.eq(2).attr('colspan')).toBe('1');
        });

        it("should have the correct number of rows", function () {
            var table = element.find('tbody');
            var trs = table.find('tr');
            expect(trs.length).toBe(3);
        });

        it("should have the correct row data", function () {
            var table = element.find('tbody');
            var trs = table.find('tr');
            var row1 = trs.eq(0);
            expect(row1.find('td').eq(0).find('span').html()).toBe('100%');
            expect(row1.find('td').eq(1).find('span').html()).toBe('#example 1');
            expect(row1.find('td').eq(2).find('span').html()).toBe('20 Feb `79 - 06:00:00');

            var row2 = trs.eq(1);
            expect(row2.find('td').eq(0).find('span').html()).toBe('85%');
            expect(row2.find('td').eq(1).find('span').html()).toBe('#example 2');
            expect(row2.find('td').eq(2).find('span').html()).toBe('02 Jan `07 - 06:00:00');

            var row3 = trs.eq(2);
            expect(row3.find('td').eq(0).find('span').html()).toBe('70%');
            expect(row3.find('td').eq(1).find('span').html()).toBe('#example 3');
            expect(row3.find('td').eq(2).find('span').html()).toBe('30 Oct `08 - 06:00:00');
        });

        it("should have the correct colspan in the body", function () {
            var table = element.find('tbody');
            var trs = table.find('tr');
            expect(trs.length).toBe(3);
            var tds = trs.eq(0).find('td');
            expect(tds.eq(0).attr('colspan')).toBe('3');
            expect(tds.eq(1).attr('colspan')).toBe('1');
            expect(tds.eq(2).attr('colspan')).toBe('1');
        });
    });

    describe('that is provided valid data but specified that there is no header', function () {
        var element;

        beforeEach(function () {
            scope.config = factory.createDatatableConfig()
                .addColumn('value', 'Value - Title', 'icon', '{{row.value}} - #{{row.title}}', false, 2)
                .addColumn('timestamp', 'Build date', '', '{{row.timestamp | date:\'dd MMM `yy - HH:mm:ss\'}}', false)
                .addRows(data);

            element = angular.element(
                '<div>' +
                '   <datatable config="config" class="table table-normal"/>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();
        });

        it("should set the correct style class", function () {
            var table = element.find('table');
            expect(table.length).toBe(1);
            expect(table.eq(0).attr('class')).toContain('table table-normal');
        });

        it("should set the correct number of columns", function () {
            var table = element.find('thead');
            var trs = table.find('tr');
            expect(trs.length).toBe(1);
            var tds = trs.find('td');
            expect(tds.length).toBe(0);
        });

        it("should set the correct number of rows", function () {
            var table = element.find('tbody');
            var trs = table.find('tr');
            expect(trs.length).toBe(3);
        });

        it("should set the correct row data", function () {
            var table = element.find('tbody');
            var trs = table.find('tr');
            var row1 = trs.eq(0);
            expect(row1.find('td').eq(0).find('span').html()).toBe('100% - #example 1');
            expect(row1.find('td').eq(1).find('span').html()).toBe('20 Feb `79 - 06:00:00');

            var row2 = trs.eq(1);
            expect(row2.find('td').eq(0).find('span').html()).toBe('85% - #example 2');
            expect(row2.find('td').eq(1).find('span').html()).toBe('02 Jan `07 - 06:00:00');

            var row3 = trs.eq(2);
            expect(row3.find('td').eq(0).find('span').html()).toBe('70% - #example 3');
            expect(row3.find('td').eq(1).find('span').html()).toBe('30 Oct `08 - 06:00:00');
        });
    });
});
