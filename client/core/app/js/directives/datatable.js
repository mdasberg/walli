"use strict";

/* The datatable directive. */

angular.module('core').directive('datatable', function () {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            config: '='
        },
        replace: true,
        templateUrl: 'partials/datatable.html'
    }
});