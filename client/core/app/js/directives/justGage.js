"use strict";

/* The just gage directive wraps the Justgage library. */

/* global JustGage, $timeout */
angular.module('core').directive('justGage', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        scope: {
            identifier: '=',
            title: '=',
            widthScale: '=',
            value: '=',
            min: '=',
            max: '=',
            levelColors: '=',
            refreshAnimationTime: '=',
            refreshAnimationType: '='
        },
        replace: true,
        templateUrl: 'partials/justGage.html',
        link: function (scope, element, attrs) {
            var config = {
                id: (angular.isDefined(scope.identifier) ? scope.identifier : scope.$$hashKey) + '-justgage',
                title: (angular.isDefined(scope.title) ? scope.title : undefined),
                gaugeWidthScale: (angular.isDefined(scope.widthScale) ? scope.widthScale : 0.2),
                levelColors: (angular.isDefined(scope.levelColors) ? scope.levelColors : []),
                value: (angular.isDefined(scope.value) ? scope.value : 0),
                min: (angular.isDefined(scope.min) ? scope.min : 0),
                max: (angular.isDefined(scope.max) ? scope.max : 100),
                refreshAnimationTime: (angular.isDefined(scope.refreshAnimationTime) ? scope.refreshAnimationTime : 4000),
                refreshAnimationType: (angular.isDefined(scope.refreshAnimationType) ? scope.refreshAnimationType : 'bounce')
            };

            $timeout(function () { // wait for the element to be drawn

                if (element.children().length === 0) { // only add it once.
                    var g = new JustGage(config);

                    scope.$watch('value', function (value) {
                        $timeout(function () {
                            g.refresh(value);
                        }, 100);
                    });
                }
            });
        }
    };
}]);