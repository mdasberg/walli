"use strict";

/* The control group directive. */

angular.module('core').directive('controlGroup', function () {
    return {
        restrict: 'E',
        templateUrl: 'partials/controlGroup.html',
        replace: true,
        scope: true,
        transclude: true,
        link: function (scope, element, attrs) {
            scope.id = attrs.id;
            scope.label = attrs.label;
        }
    };
});

