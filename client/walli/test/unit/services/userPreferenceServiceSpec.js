"use strict";

/* jasmine specs for the userPreference service. */
describe('The userPreferencesService', function () {
    var service;

    beforeEach(function () {
        module('walli');
        localStorage.clear();
        inject(function (userPreferenceService) {
            localStorage.setItem('walli-projects-prefs', angular.toJson({
                "1": {
                    "displayName": "first-example",
                    "sonar": {
                        "viewMode": [
                            {"name": "overview", "checked": true},
                            {"name": "details", "checked": false}
                        ]},
                    "stash": {},
                    "jenkins": {},
                    "github": {},
                    "bitbucket": {
                        "branch": {
                            "name": "master"
                        }
                    }
                },
                "2": {
                    "displayName": "second-example",
                    "sonar": {
                        "viewMode": [
                            {"name": "overview", "checked": true},
                            {"name": "details", "checked": false}
                        ]},
                    "stash": {},
                    "jenkins": {},
                    "github": {},
                    "bitbucket": {
                        "branch": {
                            "name": "master"
                        }
                    }
                }
            }));

            service = userPreferenceService;
        })
    });

    it("should add the preferences to the local storage", function () {
        localStorage.clear();
        expect(localStorage.getItem('walli-projects-prefs')).toBe(null);

        service.retrieveProjectsPreferences();

        expect(localStorage.getItem('walli-projects-prefs')).toBe(angular.toJson({}));
    });
    it("should get the preferences", function () {
        var response = service.retrieveProjectsPreferences();

        expect(response[1].displayName).toBe('first-example');
        expect(response[2].displayName).toBe('second-example');
    });
    it("should get a preference", function () {
        expect(service.retrieveProjectPreferences('1').displayName).toBe('first-example');
        expect(service.retrieveProjectPreferences('2').displayName).toBe('second-example');
    });
    it("should not get a preference", function () {
        expect(service.retrieveProjectPreferences('3')).toEqual({});
    });
    it("should update a preference", function () {
        var response = service.retrieveProjectPreferences('1');
        expect(response.bitbucket.branch.name).toBe('master');

        response.bitbucket.branch.name = 'develop';

        service.storeProjectPreferences('1', response);

        var response = service.retrieveProjectPreferences('1');
        expect(response.bitbucket.branch.name).toBe('develop');
    });
});


