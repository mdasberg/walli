"use strict";

/* jasmine specs for the proxy service. */
describe('The proxyService', function () {
    beforeEach(module('walli'));

        it("should actualy call the resource", inject(function($httpBackend, proxyService) {
            $httpBackend.when('GET', '/api/proxy/sonar/query').respond(stubs.services.proxy, { 'Content-type': 'application/json' });

            var service = proxyService.query({source: 'sonar', query: 'query'});
            $httpBackend.flush();

            expect(angular.equals(service, stubs.services.proxy)).toBeTruthy();
        }));
});
