"use strict";

/* Authentication service. */

angular.module('walli').factory('authenticationService', ['$resource', function ($resource) {
    return $resource('/api/authentication', {}, {
        login: {
            method: 'POST'
        },
        logout: {
            method: 'DELETE'
        },
        loggedIn: {
            method: 'GET',
            isArray: false
        }
    });
}]);