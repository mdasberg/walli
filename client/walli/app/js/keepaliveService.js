"use strict";

/* Keepalive service. */

angular.module('walli').factory('keepaliveService', ['$resource', function ($resource) {
    return $resource('/api/proxy/:source/keepalive', {
        source: '@source'}, {
        query: {
            method: 'GET',
            isArray: false
        }
    });
}]);