"use strict";

/* User preference service. */

angular.module('walli').controller('serviceStatusCtrl', [
    '$scope',
    '$q',
    'keepaliveService',
    'sourceService',
    'modelFactory', function ($scope, $q, keepaliveService, sourceService, modelFactory) {

        var statusesConfig = modelFactory.createDatatableConfig()
            .addColumn('name', 'Name', '', '{{row.name}}', true)
            .addColumn('', 'State', 'icon', '<state-icon config=\'config.stateConfig\' state=\'row.state\'/>', true);

        var stateConfig = modelFactory.createStateConfig()
            .addState('up', 'icon-ok status-success')
            .addState('down', 'icon-remove status-error', true);

        statusesConfig.stateConfig = stateConfig;

        sourceService.query({},
            function (sources) {
                statusesConfig.addRow({name: 'Database', state: 'up'});
                var sourceStatuses = [];
                angular.forEach(sources, function (source) {
                    sourceStatuses.push(sourceStatus(source.name));
                });

                $q.all(sourceStatuses)
                    .then(function (data) {
                        angular.forEach(data, function (sourceStatus) {
                            statusesConfig.addRow(sourceStatus);
                        });
                    });
                $scope.statusesConfig = statusesConfig;
            }, function (response) {
                statusesConfig.addRow({name: 'Database', state: 'down'})
                $scope.statusesConfig = statusesConfig;
            });

        /**
         * Get the source status.
         * @param source The source.
         * @returns {*}
         */
        function sourceStatus(source) {
            var deferred = $q.defer();
            keepaliveService.get({source: source}, function (data) {
                deferred.resolve({name: source, state: 'up'});
            }, function (response) {
                deferred.resolve({name: source, state: 'down'});
            });
            return deferred.promise;
        }
    }]);