"use strict";

/* Controller that handles the fetching of the current project for the overview. */

angular.module('walli').controller('overviewCtrl', [
    '$scope',
    '$rootScope',
    '$routeParams',
    '$q',
    'projectService',
    'userPreferenceService', function ($scope, $rootScope, $routeParams, $q, projectService, userPreferenceService) {

        function getProject(projectId) {
            var deferred = $q.defer();
            projectService.get({id: projectId}, function (project) {
                deferred.resolve(project);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        }

        getProject($routeParams.projectId).then(function (project) {
            var projectPreferences = userPreferenceService.retrieveProjectPreferences(project.id);
            projectPreferences.displayName = project.displayName;
            angular.forEach(project.infos, function (info) {
                var currentPreference = projectPreferences[info.name];
                if (!angular.isDefined(currentPreference)) {
                    projectPreferences[info.name] = {};
                }
            });
            userPreferenceService.storeProjectPreferences($routeParams.projectId, projectPreferences);

            setTimeout(function () {
                $rootScope.$broadcast('project', project);
            }, 10);
        });
    }]);