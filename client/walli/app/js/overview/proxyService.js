"use strict";

/*
 * Proxy service is used to proxy requests through the server which
 * takes care of authentication.
 */

angular.module('walli').factory('proxyService', ['$resource', function ($resource) {
    var blaat;
    return $resource('/api/proxy/:source/:query', {
        source: '@source',
        query: '@query'
    }, {
        query: {
            method: 'GET',
            isArray: false
        },
        queryRefresh: {
            method: 'GET',
            isArray: false,
            headers: {'refresh': true}
        },
        get: {
            method: 'GET',
            isArray: true
        },
        getRefresh: {
            method: 'GET',
            isArray: true,
            headers: {'refresh': true}
        }
    });
}]);