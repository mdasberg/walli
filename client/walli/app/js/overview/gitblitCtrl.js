"use strict";

/* Controller that handles connecting with and processing data from gitblit. */

/* global _, $ */
angular.module('walli').controller('gitblitCtrl', [
    '$scope',
    '$routeParams',
    '$q',
    'projectService',
    'proxyService',
    'userPreferenceService',
    'modelFactory',
    'utils', function ($scope, $routeParams, $q, projectService, proxyService, userPreferenceService, modelFactory, utils) {
        var projectId = $routeParams.projectId;
        $scope.online = true;
        $scope.source = "gitblit";

        var tooltip = document.createElement('div'),
            leftOffset = -($('html').css('padding-left').replace('px', '') + $('body').css('margin-left').replace('px', '')),
            topOffset = -32;
        tooltip.className = 'ex-tooltip';
        document.body.appendChild(tooltip);

        $scope.$on('project', function (event, project) {
            var source = _.filter(project.infos, function (info) {
                return info.name === 'gitblit'
            })[0];

            if (source !== undefined && !_.isEmpty(source.value)) {
                var gitblitRepository = source.value;

                fetch(gitblitRepository, false);

                $scope.fetchCommits = function () {
                    commitsData(gitblitRepository, $scope.branch, false).then(function (response) {
                        var commitActivityData = modelFactory.createXChartData().yScale('linear');
                        angular.forEach(response.data, function (d) {
                            commitActivityData.addDataEntry(d.date, d.total);
                        });
                        $scope.commitActivityData = commitActivityData.data;
                    });
                    var projectPreferences = userPreferenceService.retrieveProjectPreferences(projectId);
                    projectPreferences.gitblit.branch = $scope.branch;
                    userPreferenceService.storeProjectPreferences(projectId, projectPreferences);
                };

                $scope.$on('refresh', function () {
                    fetch(gitblitRepository, true);
                });
            }
        });

        /**
         * Fetch the actual data.
         * @param gitblitRepository The Gitblit repository name.
         * @param refresh Indicator refresh.
         */
        function fetch(gitblitRepository, refresh) {
            branchesData(gitblitRepository, refresh).then(function (response) {
                var defaultBranch = _.find(response.branches, function (branch) {
                    return branch.name === 'master';
                });
                var currentBranch = userPreferenceService.retrieveProjectPreferences(projectId).gitblit.branch;
                var currentBranchName = angular.isDefined(currentBranch) ? currentBranch.name : undefined;

                var found = _.find(response.branches, function (branch) {
                    return branch.name === currentBranchName;
                });
                var projectPreferences = userPreferenceService.retrieveProjectPreferences(projectId);

                if (!found) {
                    projectPreferences.gitblit.branch = {name: defaultBranch.name};
                    $scope.branch = defaultBranch;
                } else {
                    projectPreferences.gitblit.branch = found;
                    $scope.branch = found;
                }
                userPreferenceService.storeProjectPreferences(projectId, projectPreferences);

                commitsData(gitblitRepository, $scope.branch, refresh).then(function (response) {
                    var commitActivityData = modelFactory.createXChartData().yScale('linear');
                    angular.forEach(response.data, function (d) {
                        commitActivityData.addDataEntry(d.date, d.total);
                    });
                    $scope.commitActivityData = commitActivityData.data;
                });

                $scope.branches = response.branches;

                $scope.commitActivityChartOptions = modelFactory.createXChartOptions()
                    .paddingLeft(35)
                    .paddingRight(10)
                    .paddingTop(10)
                    .axisPaddingLeft(5)
                    .tickHintY(4)
                    .mouseover(function (d, i) {
                        var pos = $(this).offset();
                        $(tooltip).text("Commit activity on " + d.x + " is: " + d.y)
                            .css({top: topOffset + pos.top, left: pos.left + leftOffset})
                            .show();
                    })
                    .mouseout(function (x) {
                        $(tooltip).hide();
                    })
                    .options;
                $scope.online = true;

            }, function (response) {
                $scope.online = false;
            });
        }

        /**
         * Get the commits.
         * @param repository The repository.
         * @param branch The branch.
         * @param refresh Indicator refresh.
         * @return {*}
         */
        function commitsData(repository, branch, refresh) {
            var deferred = $q.defer();

            var success = function (response) {
                deferred.resolve({name: repository, data: getTimelineData(response)});
            };

            var error = function (response) {
                deferred.reject({failed: true, status: response.status});
            };

            if (refresh) {
                proxyService.getRefresh({
                    source: 'gitblit',
                    query: 'feed/' + repository + '?h=refs/heads/' + branch.name
                }, success, error);
            } else {
                proxyService.get({
                    source: 'gitblit',
                    query: 'feed/' + repository + '?h=refs/heads/' + branch.name
                }, success, error);
            }
            return deferred.promise;
        }

        /**
         * Get the branches.
         * @param repository The repository.
         * @param refresh Indicator refresh.
         * @return {*}
         */
        function branchesData(repository, refresh) {
            var deferred = $q.defer();

            var success = function (response) {
                var branches = [];
                angular.forEach(response, function (repo, url) {
                    if (repository === repo.name) {
                        angular.forEach(repo.availableRefs, function (ref) {
                            if (ref.substring(0, 10) === 'refs/heads') {
                                branches.push({name: ref.replace('refs/heads/', '')});
                            }
                        });
                    }
                });
                deferred.resolve({name: repository, branches: branches});
            };

            var error = function (response) {
                deferred.reject({failed: true, status: response.status});
            };

            if (refresh) {
                proxyService.queryRefresh({
                    source: 'gitblit',
                    query: 'rpc?req=LIST_REPOSITORIES'
                }, success, error);
            } else {
                proxyService.query({
                    source: 'gitblit',
                    query: 'rpc?req=LIST_REPOSITORIES'
                }, success, error);
            }

            return deferred.promise;
        }

        /**
         * Get the timeline data.
         * @param commitData The commit data.
         * @return {Array}
         */
        function getTimelineData(commits) {

            var commitData = [],
                timelineData = [];
            // commits by date.
            angular.forEach(commits, function (commit) {
                commitData.push({
                    date: utils.dayTimestamp(commit.date)
                });
            });

            var groupedByDate = _.chain(commitData).groupBy('date').sortBy(function (commit) {
                return commit.date;
            }).value();

            angular.forEach(groupedByDate, function (commitsByDate) {
                timelineData.push({date: utils.fullDate(commitsByDate[0].date), total: _.groupBy(commitsByDate, 'name').undefined.length});
            });
            return timelineData.splice(0, 15);
        }
    }]);
