"use strict";

/* Controller that gets all the projects. */

angular.module('walli').controller('projectsCtrl', [
    '$scope',
    'projectService',
    'sourceService',
    'userPreferenceService',
    'modelFactory', function ($scope, projectService, sourceService, userPreferenceService, modelFactory) {

        $scope.projects = projectService.query({}, function (projects) {
            var projectsConfig = modelFactory.createDatatableConfig()
                .addColumn('displayName', 'Name', '', '{{row.displayName}}', true)
                .addColumn('', 'Opts', 'icon', '<a href="#/projects/{{row.id}}"><i class="icon-pencil"></i></a>', true)
                .addRows($scope.projects);

            $scope.projectsConfig = projectsConfig;
        });

        $scope.sources = sourceService.query();

        /* Indicates if the project has the given source. */
        $scope.hasSource = function (project, source) {
            var usesSource = false;
            angular.forEach(project.infos, function (info) {
                if (info.name === source.name) {
                    usesSource = info.value.length > 0;
                }
            });
            return usesSource;
        }

    }]);

