"use strict";

/* Controller that handles the creation of a project. */

/* global _ */
angular.module('walli').controller('projectCreateCtrl', [
    '$scope',
    '$q',
    '$location',
    'projectService',
    'sourceService',
    'modelFactory', function ($scope, $q, $location, projectService, sourceService, modelFactory) {
        $scope.project = {infos: []};

        $scope.stateConfig = modelFactory.createStateConfig()
            .addState(true, 'icon-ok status-success')
            .addState(false, 'icon-remove status-error', true);

        /* Indicate the project name is unique. */
        $scope.uniqueProject = function () {
            var defer = $q.defer();
            projectService.query({}, function (projects) {
                var found = _.find(projects, function (p) {
                    return p.name === $scope.project.name;
                });
                defer.resolve(found === undefined);
            });
            return defer.promise;
        };

        function getSources() {
            var deferred = $q.defer();
            sourceService.query({}, function (data) {
                deferred.resolve(data);
            }, function (response) {
                deferred.reject();
            });
            return deferred.promise;
        }

        getSources().then(function (sources) {
            $scope.sources = sources;
            angular.forEach($scope.sources, function (source) {
                $scope.project.infos.push({"id": 0, "name": source.name, "value": ""})
            });
        });

        /* Save the project. */
        $scope.save = function () {
            projectService.save($scope.project, function () {
                $location.path('/settings');
            });
        }
    }]);